#!/bin/bash

source colour_coding.sh

echo ""
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
echo "                      ${txtcyn}${txtbld}Diff Command Program${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
echo ""
echo "${txtcyn}${txtbld}Enter the first file to condcut a series of comparisons: ${txtrst}"
read file1
echo ""
echo "${txtcyn}${txtbld}Enter the second file to conduct a comparison:${txtrst}"
read file2
echo ""
echo "${txtcyn}${txtbld}The following output will only confirm if the files are different:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff -q $file1 $file2
echo ""
echo "${txtcyn}${txtbld}TThe following output will confirm if the files are the same:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff -s $file1 $file2
echo ""
echo "${txtcyn}${txtbld}The following will output the differences in two columns:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff -y $file1 $file2
echo ""
echo "${txtcyn}${txtbld}The following output will omit common lines:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff --suppress-common-lines $file1 $file2
echo ""
echo "${txtcyn}${txtbld}The following will ignore case when comparing file names:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff --ignore-file-name-case $file1 $file2
echo ""
echo "${txtcyn}${txtbld}Consider case when comparing file names:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff --no-ignore-file-name-case $file1 $file2
echo ""
echo "${txtcyn}${txtbld}Ignore case differences in file contents:${txtrst}"
echo "${txtcyn}${txtbld}=============================================================${txtrst}"
diff -i $file1 $file2
echo ""
