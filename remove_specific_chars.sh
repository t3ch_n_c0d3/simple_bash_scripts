#!/bin/bash

# The line below allows the colour_coding.sh file to be imported into this file.
source colour_coding.sh
# reference: www.theunixschool.com/2014/08/sed-examples-remove-delete-chars-from-line-file.html

echo "${txtcyn}${txtbld}===============================================================${txtrst}"
echo "                   ${txtcyn}${txtbld}Character Deletion Program"
echo "${txtcyn}${txtbld}================================================================${txtrst}"
echo ""
echo "${txtcyn}${txtbld}To remove the first occurrence of a  specific character in a file${txtrst}"
echo "${txtcyn}${txtbld}you will need to enter the character you wish to delete from a file.${txtrst}"
read char
echo ""
echo "${txtcyn}${txtbld}Enter the name of the file to edit: ${txtrst}"
read file
echo ""
echo "${txtcyn}${txtbld}Output"
echo "${txtcyn}${txtbld}===============================================================${txtrst}"
sed 's/'${char}'//' $file
echo ""

