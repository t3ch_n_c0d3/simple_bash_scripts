#!/bin/bash

# Link to tutorial for this script
# https://www.floppix.com/scripts1.html

COLOR=blue
setterm -background $COLOR

# I have modified this line due to a technical conflict in how the script will be read if quotes are not included.
echo "It is a $COLOR day."
