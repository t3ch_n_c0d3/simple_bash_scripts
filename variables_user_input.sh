#!/bin/bash

# ===========================================================
# Original source code: https://www.floppix.com/scripts1.html
# ===========================================================

# Request input from user
echo  -n "Pick a screen color (blue, yellow, red): "
read -e COLOR 

# Set background color based upon user input:
setterm -background $COLOR
echo "It is a $COLOR day."
